import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBException;
import java.io.StringReader;
import java.io.StringWriter;

public class XMLParser implements Marshaller<User> {
    @Override
    public User read(String text) throws JAXBException {
        StringReader reader = new StringReader(text);
        User user = JAXB.unmarshal(reader, User.class);

        return user;
    }

    @Override
    public String write(User item) throws JAXBException {
        StringWriter writer = new StringWriter();
        JAXB.marshal(item, writer);

        return writer.toString();
    }
}
