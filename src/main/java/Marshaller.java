import javax.xml.bind.JAXBException;

public interface Marshaller <T> {
    T read(String text) throws JAXBException;

    String write(T item) throws JAXBException;
}
