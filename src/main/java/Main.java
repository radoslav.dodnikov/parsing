import javax.xml.bind.JAXBException;

public class Main {
    public static void main(String[] args) throws JAXBException {

        String jsonText = "{\n" +
                "\n" +
                "\t\"name\" : \"Radoslav\",\n" +
                "\n" +
                "\t\"age\" : 28,\n" +
                "\n" +
                "\t\"details\" : \"Java Developer\"\n" +
                "\n" +
                "}";


        JSONParser jsonParser = new JSONParser();
        User jsonUser = jsonParser.read(jsonText);
        System.out.println(jsonUser);
        String jsonResult = jsonParser.write(jsonUser);
        System.out.println(jsonResult);

        String xmlText = "<user>\n" +
                "\n" +
                "\t<name>Rado</name>\n" +
                "\n" +
                "\t<age>27</age>\n" +
                "\n" +
                "\t<details>Java Knights student</details>\n" +
                "\n" +
                "</user>";

        XMLParser xmlParsing = new XMLParser();
        try {
            User xmlUser = xmlParsing.read(xmlText);
            System.out.println(xmlUser);
            String xmlResult = xmlParsing.write(xmlUser);
            System.out.println(xmlResult);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
