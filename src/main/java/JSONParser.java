import com.google.gson.Gson;

import javax.xml.bind.JAXBException;

public class JSONParser implements Marshaller<User> {
    private Gson gson;

    JSONParser() {
        this.gson = new Gson();
    }

    @Override
    public User read(String text) throws JAXBException {
        User user = this.gson.fromJson(text, User.class);

        return user;
    }

    @Override
    public String write(User item) throws JAXBException {
        String json = this.gson.toJson(item);

        return json;
    }
}
